#include <stdio.h>
#include "image_ppm.h"
#include <fstream>

/*
Ecrire un programme profil.cpp permettant d’afficher à l’écran sur 2 colonnes les indices et
les niveaux de gris d'une ligne ou d'une colonne d'une image. Ce programme aura comme
arguments, le nom l’image, une information précisant s'il s'agit d'une ligne ou d'une colonne, et
un indice indiquant le numéro de la ligne ou de la colonne.

Au lieu d’afficher les valeurs des pixels d’une ligne ou d’une colonne à l’écran, nous souhaitons
maintenant les visualiser sous forme de courbes à l’aide du logiciel GNUPLOT.
Pour cela, il faut rediriger l’affichage de l’écran vers un fichier que nous appellerons
profil.dat qui sera ensuite visualisé avec le logiciel Gnuplot.
Visualiser les profils d'une ligne et d'une colonne d’une image au format pgm indiquée par
l’enseignant.
A l'aide du logiciel GNUPLOT, pour visualiser une courbe :
> plot 'fich.dat' with lines
*/

/*Arguments : nom_de_image ligne_ou_colonne indice_ligne_ou_colonne*/
int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH,nW,nTaille;
  char info;
  int indice;

  
  if (argc != 4) 
     {
       printf("Usage: cheminImage info indice \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",&info);
   sscanf (argv[3],"%d",&indice);
   
   

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	
  //si on va traiter une colonne 
  //si on traite la colonne c'est la ligne qui va varier entre 0 et nH
  if(info=='c')
  {
      int tableau[nH];

      for (int j=0; j<nH ; j++)
	      {
	        tableau[j] = ImgIn[j*nW+indice];
	        printf("%i \t :\t %i\n", j, tableau[j]);
	      }
  
    std::ofstream fichier ("profil.dat");
      for (int i=0; i<nH; i++)
        {
	        fichier << i <<"\t" <<tableau[i] <<"\n";
        }
        fichier.close();
  }else{ //si je traite une ligne c'est l'indice des colonnes qui va varier entre 0 et nW
      int tab[nW];

      for (int i=0; i<nW ; i++)
	    {
	      tab[i] = ImgIn[indice*nW+i];
	      printf("%i \t : \t %i \n", i, tab[i]);
	    }
      std::ofstream fichier ("profil.dat");

      for (int i=0; i<nH; i++){
	      fichier <<i<<"\t"<<tab[i]<<"\n";
      }
      fichier.close();
    }
  
}