// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <fstream>
#include "image_ppm.h"
#include <iostream>

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille;
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm  \n"); 
       exit (1) ;
     }
sscanf (argv[1],"%s",cNomImgLue) ;
int occ[256];
for ( int i=0; i<256; i++){
	occ[i]=0;
}

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);


   

std::ofstream fichier ("histoddp.dat");
int ddp[256];
float f[256];
for (int i=0; i < nH; i++){
    for (int j=0; j < nW; j++)
     {
     //occ[ImgIn[i*nW+j]] = occ[ImgIn[i*nW+j]]+1;
       ddp[occ[ImgIn[i*nW+j]]/nH*nW] =ddp[occ[ImgIn[i*nW+j]]+1/nH*nW];
       f[0] = ddp[0];
       f[i] = f[i-1] + ddp[i];
       //ddp[ImgIn[i*nW+j]/nH*nW] =ddp[ImgIn[i*nW+j]+1/nH*nW];
    
     }
}
    


for ( int i=0; i<256; i++){
  
  
    
	fichier<<i<<"   "<<f[i]<<"\n";
}

free(ImgIn);
//fichier.close();
}

