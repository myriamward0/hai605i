// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille;
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm  \n"); 
       exit (1) ;
     }
sscanf (argv[1],"%s",cNomImgLue) ;
int hist[256];
for ( int i=0; i<256; i++){
	hist[i]=0;
}

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);


   

std::ofstream fichier ("histo.dat");

for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
       hist[ImgIn[i*nW+j]] = hist[ImgIn[i*nW+j]]+1;
	
	
     }

for ( int i=0; i<256; i++){
	fichier<<i<<"   "<<hist[i]<<"\n";
}
free(ImgIn);
//fichier.close();
}

