// test_couleur.cpp : Seuille une image en niveau de gris
#include <fstream>
#include <stdio.h>
#include "image_ppm.h"
#include <cmath>


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH,nW,nTaille,SH, SB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm  ImageOut.pgm SB SH\n"); 
       exit (1) ;
     }
   
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&SB);
  sscanf (argv[4],"%d",&SH);

  OCTET *ImgIn,*ImgOut;

  int Gx;
  int Gy;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   
   for(int i =1; i<nW; i++){
     for(int j =1;j<nH ; j++){
       Gx = -1*ImgIn[i*nW+j] + ImgIn[(i+1)*nW+j];
       Gy = -1*ImgIn[i*nW+j] + ImgIn[i*nW+(j+1)];

       if( sqrt(pow(Gx,2)+pow(Gy,2))<=SB)
	 ImgOut[i*nW+j] = 0;
       else if (sqrt(pow(Gx,2)+pow(Gy,2))>=SH)
	 ImgOut[i*nW+j] = 255;	   
	  
     }
   }
   

   for (int i=1; i<nW; i++){
     for (int j=1; j<nH; j++){
       if (ImgIn[i*nW+j] != 255 || ImgIn[i*nW+j] !=00){ 
	        if (ImgIn[(i-1)*nW+j] == 255  ||
          		ImgIn[(i+1)*nW+j] ==  255 ||
          		ImgIn[i*nW+(j-1)] == 255 ||
          		ImgIn[i*nW+(j+1)] == 255 ||
          		ImgIn[(i-1)*nW+(j-1)]== 255 ||
          		ImgIn[(i-1)*nW+(j+1)] ==255 ||
          		ImgIn[(i+1)*nW +(j+1)] == 255 ||
          		ImgIn[(i+1)*nW+(j-1)] == 255) {											
	 	ImgOut[i*nW+j] = 255;
	 }
       else{
	 ImgOut[i*nW+j] = 0;
       }
     }
   }
   }  


   ecrire_image_pgm(cNomImgEcrite,ImgOut, nH, nW);
   free(ImgIn); free(ImgOut);
  
}