#include <stdio.h>
#include "image_ppm.h"
#include <fstream>


int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH,nW,nTaille;
  char info;
  int indice;

  
  if (argc != 4) 
     {
       printf("Usage: cheminImage info indice \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",&info);
   sscanf (argv[3],"%d",&indice);
   
   

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	
  
  if(info=='c')
  {
      int tableau[nH];

      for (int j=0; j<nH ; j++)
	      {
	        tableau[j] = ImgIn[j*nW+indice];
	        printf("%i \t :\t %i\n", j, tableau[j]);
	      }
  
    std::ofstream fichier ("profil.dat");
      for (int i=0; i<nH; i++)
        {
	        fichier << i <<"\t" <<tableau[i] <<"\n";
        }
        fichier.close();
  }else{ 
      int tab[nW];

      for (int i=0; i<nW ; i++)
	    {
	      tab[i] = ImgIn[indice*nW+i];
	      printf("%i \t : \t %i \n", i, tab[i]);
	    }
      std::ofstream fichier ("profil.dat");

      for (int i=0; i<nH; i++){
	      fichier <<i<<"\t"<<tab[i]<<"\n";
      }
      fichier.close();
    }
  
}
