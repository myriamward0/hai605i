// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char nomImg1[250], nomImg2[250];
  int nH, nW, nTaille;
  int nR, nG, nB;
  if (argc != 3) 
     {
       printf("Usage: ImageDeBase.pgm ImageObtenue.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",nomImg1) ;
   sscanf (argv[2],"%s",nomImg2);

   OCTET *ImgIn, *ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(nomImg1, &nH, &nW);
   nTaille = nH * nW;
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(nomImg1, ImgIn, nH * nW);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(nomImg2, ImgIn2, nH * nW);
	
float erreur=0;
for (int i=0; i < nTaille; i++){
     erreur+=pow((ImgIn[i]- ImgIn2[i]),2)/(float)nTaille;
     }
printf("la valeur de EQM est de:%f \n",erreur);
 
free(ImgIn); free(ImgIn2);

   return 1;
}
