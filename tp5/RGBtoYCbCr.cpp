// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite1[250], cNomImgEcrite2[250], cNomImgEcrite3[250];
  int nH, nW, nTaille, nTaille3;
  int nR, nG, nB;
  if (argc != 5) 
     {
       printf("Usage: ImageIn.ppm ImageOut1.pgm ImageOut2.pgm ImageOut3.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite1);
   sscanf (argv[3],"%s",cNomImgEcrite2);
   sscanf (argv[4],"%s",cNomImgEcrite3);

   OCTET *ImgIn, *ImgOut1, *ImgOut2, *ImgOut3;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
   nTaille3= 3 * nTaille;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nW * nH);
   allocation_tableau(ImgOut1, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);
   allocation_tableau(ImgOut3, OCTET, nTaille);
	
for (int i=0; i < nTaille3; i+=3){
    nR = ImgIn[i];
    nG = ImgIn[i+1];
    nB = ImgIn[i+2];

    ImgOut1[i/3] = (0.299*nR + 0.587*nG + 0.114*nB);

    ImgOut2[i/3] = (-0.1687*nR - 0.3313 *nG + 0.5*nB + 128);

    ImgOut3[i/3] = (0.5*nR - 0.4187*nG - 0.0813*nB + 128);
}
 
   ecrire_image_pgm(cNomImgEcrite1, ImgOut1,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite2, ImgOut2,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite3, ImgOut3,  nH, nW);

   free(ImgIn); 

   return 1;
}
