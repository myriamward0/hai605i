#include <stdio.h>
#include "image_ppm.h"

void YCbCrToRGB(OCTET* ImgY, OCTET* ImgCb, OCTET* ImgCr, OCTET* ImgOut, int nW, int nH, int composante=0) {
    for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            int y = ImgY[i*nW+j];
            int cb = ImgCb[i*nW+j];
            int cr = ImgCr[i*nW+j];
            int r = (int)(y + 1.402*(cr-128));
            int g = (int)(y - 0.344136*(cb-128) - 0.714136*(cr-128));
            int b = (int)(y + 1.772*(cb-128));
            if (composante != 0) {
                int inter;
                // the big shuffle
                if (composante == 1) { // RBG
                    inter = g;
                    g = b;
                    b = inter;
                }
                if (composante == 2) { // GRB
                    inter = g;
                    g = r;
                    r = inter;
                }
                if (composante == 3) { // GBR
                    inter = r;
                    r = g;
                    g = b;
                    b = inter;
                }
                if (composante == 4) { // BRG
                    inter = b;
                    b = g;
                    g = r;
                    r = inter;
                }
                if (composante == 5) { // BGR
                    inter = r;
                    r = b;
                    b = inter;
                }
            }
            if (r < 0) r = 0;
            if (r > 255) r = 255;
            if (g < 0) g = 0;
            if (g > 255) g = 255;
            if (b < 0) b = 0;
            if (b > 255) b = 255;
            ImgOut[3*(i*nW+j)] = r;
            ImgOut[3*(i*nW+j)+1] = g;
            ImgOut[3*(i*nW+j)+2] = b;
        }
    }
}

int main(int argc, char* argv[])
{
    char ImageY[250], ImageCb[250], ImageCr[250];
    int nH, nW, nTaille;

    char RGB[] = "img/rgb.ppm";
    char RBG[] = "img/rbg.ppm";
    char GRB[] = "img/grb.ppm";
    char GBR[] = "img/gbr.ppm";
    char BRG[] = "img/brg.ppm";
    char BGR[] = "img/bgr.ppm";
    
    if (argc != 4) {
        printf("Usage: ImageY.pgm ImageCb.pgm ImageCr.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",ImageY);
    sscanf (argv[2],"%s",ImageCb);
    sscanf (argv[3],"%s",ImageCr);

    OCTET *ImgY, *ImgCb, *ImgCr, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(ImageY, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgY, OCTET, nTaille);
    allocation_tableau(ImgCb, OCTET, nTaille);
    allocation_tableau(ImgCr, OCTET, nTaille);
    lire_image_pgm(ImageY, ImgY, nH * nW);
    lire_image_pgm(ImageCb, ImgCb, nH * nW);
    lire_image_pgm(ImageCr, ImgCr, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille*3);

    YCbCrToRGB(ImgY, ImgCb, ImgCr, ImgOut, nW, nH, 0);
    ecrire_image_ppm(RGB, ImgOut, nH, nW);
    YCbCrToRGB(ImgY, ImgCb, ImgCr, ImgOut, nW, nH, 1);
    ecrire_image_ppm(RBG, ImgOut, nH, nW);
    YCbCrToRGB(ImgY, ImgCb, ImgCr, ImgOut, nW, nH, 2);
    ecrire_image_ppm(GRB, ImgOut, nH, nW);
    YCbCrToRGB(ImgY, ImgCb, ImgCr, ImgOut, nW, nH, 3);
    ecrire_image_ppm(GBR, ImgOut, nH, nW);
    YCbCrToRGB(ImgY, ImgCb, ImgCr, ImgOut, nW, nH, 4);
    ecrire_image_ppm(BRG, ImgOut, nH, nW);
    YCbCrToRGB(ImgY, ImgCb, ImgCr, ImgOut, nW, nH, 5);
    ecrire_image_ppm(BGR, ImgOut, nH, nW);

    free(ImgY); free(ImgCb); free(ImgCr); free(ImgOut);

    return 1;
}