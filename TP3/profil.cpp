// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250];
    char cColLine[15];
    int nH, nW, nTaille, indice;
    
    if (argc != 4) 
        {
        printf("Usage: ImageIn.pgm [Line|Col] indice\n"); 
        exit (1) ;
        }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cColLine) ;
    sscanf (argv[3],"%d",&indice) ;

    if (strcmp(cColLine, "col") && strcmp(cColLine, "line")) {
        printf("écrire <<col>> ou <<line>>\n");
        exit(1);
    }

    OCTET *ImgIn;
    int *Profil;
    int maxGris = 256;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(Profil, int, maxGris);

    int maxiCoul = 0;
    
    if (strcmp(cColLine, "col") == 0) {
        for(int y=0; y<nH; y++) {
            Profil[y] = ImgIn[y*nW + indice];
        }
    } else {
        for(int x=0; x<nH; x++) {
            Profil[x] = ImgIn[indice*nW + x];
        }
    }
    
    for(int i=0; i<maxGris; i++) {
        printf("%i %i\n",i,Profil[i]);
    }

    free(ImgIn); 
    free(Profil);
    
    return 1;
}
